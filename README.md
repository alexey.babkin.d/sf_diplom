# Работа с проектом

1. Создание инфраструктуры с помощью [terraform](terraform/).
2. Настройка серверов с помощью [ansible](ansible/).
3. Регистрация runner на [gitlab](django/).
4. Алертинг
Для настройки оповещений нужно получить новый токен в `@MiddlemanBot (https://t.me/MiddlemanBot)`
и вписать его в ansible/roles/srv/templates/alertmanager.yml.j2
5. Логи
5.1 Добовляемя репозиторий
```sh
helm repo add grafana https://grafana.github.io/helm-charts
```
5.2 Устанавливаем все стек
```sh
helm install loki grafana/loki-stack--namespace loki --create-namespace --set grafana.enabled=true
```
5.3 Поулчаем пароль адми графаны
```sh
kubectl get secret --namespace loki loki-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```
5.4 Пробрасываем порт графаны
```sh
kubectl port-forward --namespace loki service/loki-grafana 3124:80
```
5.5 Смотрим логи через Explore
пример вывода всех логов
```sh
{namespace="default"}
```
