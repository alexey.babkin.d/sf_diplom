# CI/CD
1. Регистрируем раннер
```sh
gitlab-runner register
```
2. Добавляем переменные в CI/CD
```sh
DOCKER_USERNAME='username_for_dockerhub'
DOCKER_PASSWORD='pass_for_dockerhub'
```
3. Копируем /etc/kubernetes/admin.conf с мастера kubernetes в такой же файл на srv
