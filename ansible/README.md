# Ansible
1. Меняем ip адреса в inventory.yml
2. Качаем деб пакеты графаны с нашего облака 
```sh
https://disk.yandex.ru/d/qh6_RLKh2DT4Ew
```
3. Копируем пакет графаны в папку с ansible
```sh
cp grafana_9.1.4_amd64.deb ansible/grafana_9.1.4_amd64.deb
```
4. Запускаем asnible
```sh
ansible-playbook -i inventory.yml start_roles.yml
```

